/*
 * @author <a href="mailto:ivzyl@edu.hse.ru"> Iryna Zyl </a>
 */

package annotations.processors;

import java.lang.annotation.Annotation;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Процессор, обрабатывающий аннотацию NotEmpty.
 */
public class NotEmptyProcessor implements Processor {
    public void process(Annotation annotation, Object obj) throws IllegalArgumentException {
        // Если не тот тип.
        if (!(obj == null || obj instanceof List<?> || obj instanceof Set<?> || obj instanceof Map<?, ?> ||
                obj instanceof String)) {
            throw new IllegalArgumentException("Object of this type can't have a NotEmpty Annotation");
        }

        // Если не то значение.
        if (!(obj == null || (obj instanceof List<?> && ((List<?>) obj).size() != 0) ||
                (obj instanceof Set<?> && ((Set<?>) obj).size() != 0) ||
                (obj instanceof Map<?, ?> && ((Map<?, ?>) obj).size() != 0) ||
                (obj instanceof String && ((String) obj).length() != 0))) {
            throw new IllegalArgumentException("Can't be empty");
        }
    }
}
