/*
 * @author <a href="mailto:ivzyl@edu.hse.ru"> Iryna Zyl </a>
 */

package annotations.processors;

import annotations.AnyOf;

import java.lang.annotation.Annotation;
import java.util.Arrays;

/**
 * Процессор, обрабатывающий аннотацию AnyOf.
 */
public class AnyOfProcessor implements Processor {

    public void process(Annotation annotation, Object obj) throws IllegalArgumentException {
        AnyOf ann = (AnyOf) annotation;

        // Если не тот тип.
        if (!(obj == null || obj instanceof String)) {
            throw new IllegalArgumentException("Object of this type can't have an AnyOf Annotation");
        }

        // Если не то значение.
        if (!(obj == null || Arrays.asList(ann.value()).contains(obj))) {
            throw new IllegalArgumentException("Must be one of the: " + getValidValues(ann));
        }
    }

    /**
     * Превращает список в строку-перечисление элементов.
     *
     * @param annotation Аннотация в которой мы берём список.
     * @return Срока-представление.
     */
    private String getValidValues(AnyOf annotation) {
        return "'" +
                String.join("', '", annotation.value()) + "'";
    }
}
