/*
 * @author <a href="mailto:ivzyl@edu.hse.ru"> Iryna Zyl </a>
 */

package annotations.processors;

import annotations.InRange;

import java.lang.annotation.Annotation;

/**
 * Процессор, обрабатывающий аннотацию InRange.
 */
public class InRangeProcessor implements Processor {

    public void process(Annotation annotation, Object obj) throws IllegalArgumentException {
        InRange ann = (InRange) annotation;

        // Если не тот тип.
        if (!(obj == null || obj instanceof Byte || obj instanceof Short || obj instanceof Integer ||
                obj instanceof Long)) {
            throw new IllegalArgumentException("Object of this type can't have an InRange Annotation");
        }

        // Если не то значение.
        if (!(obj == null ||
                (obj instanceof Byte && ((Byte) obj >= ann.min() && (Byte) obj <= ann.max())) ||
                (obj instanceof Short && ((Short) obj >= ann.min() && (Short) obj <= ann.max())) ||
                (obj instanceof Integer && ((Integer) obj >= ann.min() && (Integer) obj <= ann.max())) ||
                (obj instanceof Long && ((Long) obj >= ann.min() && (Long) obj <= ann.max())))) {

            throw new IllegalArgumentException("Value must be from " + ann.min() + " to " + ann.max());
        }
    }
}
