/*
 * @author <a href="mailto:ivzyl@edu.hse.ru"> Iryna Zyl </a>
 */

package annotations.processors;

import java.lang.annotation.Annotation;

/**
 * Процессор, обрабатывающий аннотацию NotNull.
 */
public class NotNullProcessor implements Processor {
    public void process(Annotation annotation, Object obj) throws IllegalArgumentException {
        if (obj == null) {
            throw new IllegalArgumentException("Can't be null");
        }
    }
}
