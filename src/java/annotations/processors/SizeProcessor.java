/*
 * @author <a href="mailto:ivzyl@edu.hse.ru"> Iryna Zyl </a>
 */

package annotations.processors;

import annotations.Size;

import java.lang.annotation.Annotation;
import java.util.*;

/**
 * Процессор, обрабатывающий аннотацию Size.
 */
public class SizeProcessor implements Processor {
    public void process(Annotation annotation, Object obj) throws IllegalArgumentException {
        Size ann = (Size) annotation;

        // Если не тот тип.
        if (!(obj == null || obj instanceof List<?> || obj instanceof Set<?> || obj instanceof Map<?, ?> ||
                obj instanceof String)) {
            throw new IllegalArgumentException("Object of this type can't have a Size Annotation");
        }

        // Если не то значение.
        if (!(obj == null ||
             (obj instanceof List<?> && (((List<?>) obj).size() >= ann.min() && ((List<?>) obj).size() <= ann.max())) ||
             (obj instanceof Set<?> && (((Set<?>) obj).size() >= ann.min() && ((Set<?>) obj).size() <= ann.max())) ||
             (obj instanceof Map<?, ?> &&
                  (((Map<?, ?>) obj).size() >= ann.min() && ((Map<?, ?>) obj).size() <= ann.max())) ||
             (obj instanceof String &&
                     (((String) obj).length() >= ann.min() && ((String) obj).length() <= ann.max())))) {

            throw new IllegalArgumentException("Object size must be from " + ann.min() + " to " + ann.max());
        }
    }
}
