package annotations.processors;

import java.lang.annotation.Annotation;

public interface Processor {
    void process(Annotation annotation, Object obj) throws IllegalArgumentException;
}
