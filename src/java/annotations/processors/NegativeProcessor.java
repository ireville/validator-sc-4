/*
 * @author <a href="mailto:ivzyl@edu.hse.ru"> Iryna Zyl </a>
 */

package annotations.processors;

import java.lang.annotation.Annotation;

/**
 * Процессор, обрабатывающий аннотацию Negative.
 */
public class NegativeProcessor implements Processor {
    public void process(Annotation annotation, Object obj) throws IllegalArgumentException {
        // Если не тот тип.
        if (!(obj == null || obj instanceof Byte || obj instanceof Short || obj instanceof Integer ||
                obj instanceof Long)) {
            throw new IllegalArgumentException("Object of this type can't have a Negative Annotation");
        }

        // Если не то значение.
        if (!(obj == null || (obj instanceof Byte && (Byte) obj < 0) || (obj instanceof Short && (Short) obj < 0) ||
                (obj instanceof Integer && (Integer) obj < 0) || (obj instanceof Long && (Long) obj < 0))) {
            throw new IllegalArgumentException("Must be negative");
        }
    }
}
