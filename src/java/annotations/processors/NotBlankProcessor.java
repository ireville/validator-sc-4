/*
 * @author <a href="mailto:ivzyl@edu.hse.ru"> Iryna Zyl </a>
 */

package annotations.processors;

import java.lang.annotation.Annotation;

/**
 * Процессор, обрабатывающий аннотацию NotBlank.
 */
public class NotBlankProcessor implements Processor {
    public void process(Annotation annotation, Object obj) throws IllegalArgumentException {
        // Если не тот тип.
        if (!(obj == null || obj instanceof String)) {
            throw new IllegalArgumentException("Object of this type can't have a NotBlank Annotation");
        }

        // Если не то значение.
        if (obj != null && ((String) obj).isBlank()) {
            throw new IllegalArgumentException("Can't be blank");
        }
    }
}
