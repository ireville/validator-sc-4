/*
 * @author <a href="mailto:ivzyl@edu.hse.ru"> Iryna Zyl </a>
 */

package validator;

public class FieldValidationError implements ValidationError {
    private String message;
    private String path;
    private Object failedValue;

    public FieldValidationError(String message, String path, Object failedValue) {
        this.message = message;
        this.path = path;
        this.failedValue = failedValue;
    }

    public String getMessage() {
        return message;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Object getFailedValue() {
        return failedValue;
    }
}
