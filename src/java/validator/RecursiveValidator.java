/*
 * @author <a href="mailto:ivzyl@edu.hse.ru"> Iryna Zyl </a>
 */

package validator;

import annotations.*;
import annotations.processors.*;

import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedParameterizedType;
import java.lang.reflect.AnnotatedType;
import java.lang.reflect.Field;
import java.util.*;


public class RecursiveValidator implements Validator {
    // Словарь, ставящий в соответствие аннотации процессор для её обработки.
    private static final Map<String, Processor> map = new HashMap<>();

    // Заполняем словарь.
    static {
        map.put(AnyOf.class.getSimpleName(), new AnyOfProcessor());
        map.put(InRange.class.getSimpleName(), new InRangeProcessor());
        map.put(Negative.class.getSimpleName(), new NegativeProcessor());
        map.put(NotBlank.class.getSimpleName(), new NotBlankProcessor());
        map.put(NotEmpty.class.getSimpleName(), new NotEmptyProcessor());
        map.put(NotNull.class.getSimpleName(), new NotNullProcessor());
        map.put(Positive.class.getSimpleName(), new PositiveProcessor());
        map.put(Size.class.getSimpleName(), new SizeProcessor());
    }

    /**
     * Проверяет конкретную аннотацию для объекта.
     *
     * @param annotation Проверяемая аннотация.
     * @param object     Проверяемый объект.
     * @param path       Путь до объекта.
     * @return Непустую ошибку, если что-то не так.
     */
    FieldValidationError validateOne(Annotation annotation, Object object, String path) {
        try {
            map.get(annotation.annotationType().getSimpleName()).process(annotation, object);
        } catch (IllegalArgumentException ex) {
            return new FieldValidationError(ex.getMessage(), path, object);
        }

        return null;
    }

    /**
     * Достаёт и проверяет все аннотации для текущего объекта.
     *
     * @param errors        Уже существующие ошибки, в которые допишем новые.
     * @param annotatedType Аннотированный тип текущего объекта.
     * @param field         Объект.
     * @param path          Путь до объекта.
     */
    private void processCurrentObject(Set<ValidationError> errors, AnnotatedType annotatedType, Object field,
                                      String path) {
        // Проверяем каждую аннотацию для объекта.
        for (Annotation annotation : annotatedType.getDeclaredAnnotations()) {
            FieldValidationError error = validateOne(annotation, field, path);
            if (error != null) {
                errors.add(error);
            }
        }

        // Если тип объекта пользовательский и аннотирован как проверяемый, то запускаем для него проверку.
        if (field != null && field.getClass().isAnnotationPresent(Constrained.class)) {
            Set<ValidationError> newErrors = validate(field);

            // Склеиваем пути до текущего объекта и от него до элементов с ошибками.
            for (ValidationError err : newErrors) {
                ((FieldValidationError) err).setPath(path + "." + err.getPath());
            }

            // Добавляем ошибки в общий список.
            errors.addAll(newErrors);
        }
    }

    /**
     * Рекурсивно обходит объект.
     *
     * @param errors        Уже существующие ошибки, в которые допишем новые.
     * @param annotatedType Аннотированный тип текущего объекта.
     * @param field         Объект.
     * @param path          Путь до объекта.
     */
    private void recursiveDetour(Set<ValidationError> errors, AnnotatedType annotatedType, Object field, String path) {

        // Проверяем все аннотации для этого объекта.
        processCurrentObject(errors, annotatedType, field, path);

        // Если текущий объект - лист, то запускаем проверку для каждого его элемента.
        if (annotatedType instanceof AnnotatedParameterizedType && field instanceof List<?>) {
            int i = 0;
            for (Object element : (List<?>) field) {
                AnnotatedParameterizedType paramType = (AnnotatedParameterizedType) annotatedType;
                recursiveDetour(errors, paramType.getAnnotatedActualTypeArguments()[0], element,
                        path + "[" + i + "]");
                i++;
            }
        }
    }

    /**
     * Осуществяет проверку объекта по аннотациям.
     *
     * @param object Объект.
     * @return Список ошибок.
     */
    public Set<ValidationError> validate(Object object) {
        // Создаём списиок ошибок, который будем постепенно заполнять.
        Set<ValidationError> errors = new LinkedHashSet<>();

        // Если тип не помечен, как проверяемый, то завершаем проверку.
        if (!object.getClass().isAnnotationPresent(Constrained.class)) {
            return errors;
        }

        // Иначе достаем все поля объекта и для каждого из них запускаем рекурсивную проверку.
        for (Field declaredField : object.getClass().getDeclaredFields()) {
            declaredField.setAccessible(true);
            try {
                recursiveDetour(errors, declaredField.getAnnotatedType(), declaredField.get(object),
                        declaredField.getName());
            } catch (IllegalAccessException e) {
                System.out.println("IllegalAccessException on field " + declaredField.getName());
            }
        }

        return errors;
    }
}
