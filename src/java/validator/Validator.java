/*
 * @author <a href="mailto:ivzyl@edu.hse.ru"> Iryna Zyl </a>
 */

package validator;

import java.util.Set;

public interface Validator {
    Set<ValidationError> validate(Object object);
}
