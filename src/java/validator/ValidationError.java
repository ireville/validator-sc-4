/*
 * @author <a href="mailto:ivzyl@edu.hse.ru"> Iryna Zyl </a>
 */

package validator;

public interface ValidationError {
    String getMessage();
    String getPath();
    Object getFailedValue();
}
