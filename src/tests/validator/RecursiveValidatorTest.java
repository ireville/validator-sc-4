/*
 * @author <a href="mailto:ivzyl@edu.hse.ru"> Iryna Zyl </a>
 */

package validator;

import entities.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class RecursiveValidatorTest {

    static List<GuestForm> guestForms;
    static List<String> amenities;
    static String propertyType;
    static BookingForm bookingForm;
    static RecursiveValidator validator;

    @BeforeEach
    void setUp() {
        guestForms = new ArrayList<>();
        guestForms.add(new GuestForm("Mark", "Toys", 6));
        guestForms.add(new GuestForm("Alan", "Roys", 22));
        guestForms.add(new GuestForm("Emily", "Andrews", 18));

        amenities = new ArrayList<>();
        amenities.add("TV");
        amenities.add("Kitchen");

        propertyType = "House";

        bookingForm = new BookingForm(guestForms, amenities, propertyType, new Unrelated(2));

        validator = new RecursiveValidator();
    }

    @Test
    void main() {
    }

    @Test
    void validateOne() {
        guestForms = new ArrayList<>();
        guestForms.add(new GuestForm("Mark", "Toys", -6));
        guestForms.add(new GuestForm("Alan", "", 22));
        guestForms.add(new GuestForm("", "   ", -22));
        guestForms.add(new GuestForm("Emily", "Andrews", 18));

        Field[] fields = GuestForm.class.getDeclaredFields();
        fields[0].setAccessible(true);
        fields[2].setAccessible(true);
        Annotation[] nullBlankAnnotations = fields[0].getAnnotatedType().getDeclaredAnnotations();
        Annotation[] inRangeAnnotation = fields[2].getAnnotatedType().getDeclaredAnnotations();

        assertNull(validator.validateOne(nullBlankAnnotations[0], guestForms.get(0).getFirstName(), ""));
        assertNull(validator.validateOne(nullBlankAnnotations[0], guestForms.get(1).getFirstName(), ""));
        assertNull(validator.validateOne(nullBlankAnnotations[1], guestForms.get(3).getFirstName(), ""));
        assertNotNull(validator.validateOne(nullBlankAnnotations[1], guestForms.get(2).getFirstName(), ""));

        assertNull(validator.validateOne(nullBlankAnnotations[0], guestForms.get(0).getLastName(), ""));
        assertNull(validator.validateOne(nullBlankAnnotations[0], guestForms.get(3).getLastName(), ""));
        assertNotNull(validator.validateOne(nullBlankAnnotations[1], guestForms.get(1).getLastName(), ""));
        assertNotNull(validator.validateOne(nullBlankAnnotations[1], guestForms.get(2).getLastName(), ""));

        assertNotNull(validator.validateOne(inRangeAnnotation[0], guestForms.get(0).getAge(), ""));
        assertNull(validator.validateOne(inRangeAnnotation[0], guestForms.get(1).getAge(), ""));
        assertNotNull(validator.validateOne(inRangeAnnotation[0], guestForms.get(2).getAge(), ""));
        assertNull(validator.validateOne(inRangeAnnotation[0], guestForms.get(3).getAge(), ""));
    }

    @Test
    void validate() {
        Set<ValidationError> errors = validator.validate(bookingForm);
        assertEquals(0, errors.size());

        bookingForm.setPropertyType("Apartments");
        errors = validator.validate(bookingForm);
        assertEquals(1, errors.size());
        assertEquals("Apartments", ((FieldValidationError) errors.toArray()[0]).getFailedValue());

        guestForms.get(0).setLastName("");
        errors = validator.validate(bookingForm);
        assertEquals(2, errors.size());
        assertEquals("Can't be blank", ((FieldValidationError) errors.toArray()[0]).getMessage());
        assertEquals("guests[0].lastName", ((FieldValidationError) errors.toArray()[0]).getPath());
        assertEquals("", ((FieldValidationError) errors.toArray()[0]).getFailedValue());
        assertEquals("Apartments", ((FieldValidationError) errors.toArray()[1]).getFailedValue());
        assertEquals("propertyType", ((FieldValidationError) errors.toArray()[1]).getPath());

        First first = new First();
        errors = validator.validate(first);
        assertEquals(4, errors.size());
        assertEquals("Must be positive", ((FieldValidationError) errors.toArray()[0]).getMessage());
        assertEquals("num", ((FieldValidationError) errors.toArray()[0]).getPath());
        assertEquals("Must be negative", ((FieldValidationError) errors.toArray()[1]).getMessage());
        assertEquals("list[0][0]", ((FieldValidationError) errors.toArray()[1]).getPath());
        assertEquals("Must be negative", ((FieldValidationError) errors.toArray()[2]).getMessage());
        assertEquals("list[0][2]", ((FieldValidationError) errors.toArray()[2]).getPath());
        assertEquals("Value must be from 2000 to 3000",
                ((FieldValidationError) errors.toArray()[3]).getMessage());

        Unrelated unrelated = new Unrelated(5);
        errors = validator.validate(unrelated);
        assertEquals(0, errors.size());

        bookingForm.setAmenities(null);
        errors = validator.validate(bookingForm);
        assertEquals(3, errors.size());
        assertEquals("Can't be null", ((FieldValidationError) errors.toArray()[1]).getMessage());
        assertEquals("amenities", ((FieldValidationError) errors.toArray()[1]).getPath());

        MyForm myForm = new MyForm();
        errors = validator.validate(myForm);
        assertEquals(7, errors.size());
        assertEquals("Can't be blank", ((FieldValidationError) errors.toArray()[0]).getMessage());
        assertEquals("Must be positive", ((FieldValidationError) errors.toArray()[2]).getMessage());
        assertEquals("Can't be empty", ((FieldValidationError) errors.toArray()[5]).getMessage());
        assertEquals("list[0][0]", ((FieldValidationError) errors.toArray()[6]).getPath());
    }
}