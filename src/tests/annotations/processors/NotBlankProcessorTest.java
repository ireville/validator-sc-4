/*
 * @author <a href="mailto:ivzyl@edu.hse.ru"> Iryna Zyl </a>
 */

package annotations.processors;

import annotations.NotBlank;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;

import static org.junit.jupiter.api.Assertions.*;

class NotBlankProcessorTest {
    @NotBlank
    static String str = "Hello";

    @NotBlank
    static String str2 = "";

    @NotBlank
    static Integer num = 5;

    @NotBlank
    static String str3 = "        ";

    @NotBlank
    static String str4 = null;

    @Test
    void process() {
        NotBlankProcessor processor = new NotBlankProcessor();
        Field[] fields = this.getClass().getDeclaredFields();

        assertDoesNotThrow(() -> processor.process(fields[0].getAnnotatedType().getDeclaredAnnotations()[0], str));
        assertThrows(IllegalArgumentException.class,
                () -> processor.process(fields[1].getAnnotatedType().getDeclaredAnnotations()[0], str2));

        try {
            processor.process(fields[1].getAnnotatedType().getDeclaredAnnotations()[0], str2);
        } catch (IllegalArgumentException ex) {
            assertEquals("Can't be blank", ex.getMessage());
        }

        assertThrows(IllegalArgumentException.class,
                () -> processor.process(fields[2].getAnnotatedType().getDeclaredAnnotations()[0], num));

        try {
            processor.process(fields[2].getAnnotatedType().getDeclaredAnnotations()[0], num);
        } catch (IllegalArgumentException ex) {
            assertEquals("Object of this type can't have a NotBlank Annotation", ex.getMessage());
        }

        assertThrows(IllegalArgumentException.class,
                () -> processor.process(fields[3].getAnnotatedType().getDeclaredAnnotations()[0], str3));

        try {
            processor.process(fields[3].getAnnotatedType().getDeclaredAnnotations()[0], str3);
        } catch (IllegalArgumentException ex) {
            assertEquals("Can't be blank", ex.getMessage());
        }

        assertDoesNotThrow(() -> processor.process(fields[4].getAnnotatedType().getDeclaredAnnotations()[0], str4));
    }
}