/*
 * @author <a href="mailto:ivzyl@edu.hse.ru"> Iryna Zyl </a>
 */

package annotations.processors;

import annotations.NotEmpty;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

class NotEmptyProcessorTest {
    @NotEmpty
    static String str = "Hello";

    @NotEmpty
    static String str2 = "";

    @NotEmpty
    static Integer num = 5;

    @NotEmpty
    static String str3 = "        ";

    @NotEmpty
    static String str4 = null;

    @NotEmpty
    static List<Integer> list = new ArrayList<>();

    @NotEmpty
    static Map<Long, Long> map = new HashMap<>();

    @NotEmpty
    static Set<Short> set = new LinkedHashSet<>();

    @Test
    void process() {
        NotEmptyProcessor processor = new NotEmptyProcessor();
        Field[] fields = this.getClass().getDeclaredFields();

        assertDoesNotThrow(() -> processor.process(fields[0].getAnnotatedType().getDeclaredAnnotations()[0], str));
        assertThrows(IllegalArgumentException.class,
                () -> processor.process(fields[1].getAnnotatedType().getDeclaredAnnotations()[0], str2));

        try {
            processor.process(fields[1].getAnnotatedType().getDeclaredAnnotations()[0], str2);
        } catch (IllegalArgumentException ex) {
            assertEquals("Can't be empty", ex.getMessage());
        }

        assertThrows(IllegalArgumentException.class,
                () -> processor.process(fields[2].getAnnotatedType().getDeclaredAnnotations()[0], num));

        try {
            processor.process(fields[2].getAnnotatedType().getDeclaredAnnotations()[0], num);
        } catch (IllegalArgumentException ex) {
            assertEquals("Object of this type can't have a NotEmpty Annotation", ex.getMessage());
        }

        assertThrows(IllegalArgumentException.class,
                () -> processor.process(fields[5].getAnnotatedType().getDeclaredAnnotations()[0], list));

        try {
            processor.process(fields[5].getAnnotatedType().getDeclaredAnnotations()[0], str3);
        } catch (IllegalArgumentException ex) {
            assertEquals("Can't be empty", ex.getMessage());
        }

        assertDoesNotThrow(() -> processor.process(fields[3].getAnnotatedType().getDeclaredAnnotations()[0], str3));
        assertDoesNotThrow(() -> processor.process(fields[4].getAnnotatedType().getDeclaredAnnotations()[0], str4));

        map.put(1000000L, -5L);
        assertDoesNotThrow(() -> processor.process(fields[6].getAnnotatedType().getDeclaredAnnotations()[0], map));

        assertThrows(IllegalArgumentException.class,
                () -> processor.process(fields[7].getAnnotatedType().getDeclaredAnnotations()[0], set));

        try {
            processor.process(fields[7].getAnnotatedType().getDeclaredAnnotations()[0], set);
        } catch (IllegalArgumentException ex) {
            assertEquals("Can't be empty", ex.getMessage());
        }
    }
}