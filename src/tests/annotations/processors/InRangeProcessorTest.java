/*
 * @author <a href="mailto:ivzyl@edu.hse.ru"> Iryna Zyl </a>
 */

package annotations.processors;

import annotations.InRange;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;

import static org.junit.jupiter.api.Assertions.*;

class InRangeProcessorTest {
    @InRange(min = 0, max = 100)
    static byte b = 50;

    @InRange(min = 2, max = 5)
    static Long lon = 50L;

    @InRange(min = 0, max = 100)
    static String str = "50";

    @InRange(min = 2, max = 5)
    static int integer = 3;

    @InRange(min = -20, max = 5)
    static short sh = -19;

    @InRange(min = -10, max = 10)
    static Integer num = null;

    @Test
    void process() {
        InRangeProcessor processor = new InRangeProcessor();
        Field[] fields = this.getClass().getDeclaredFields();

        assertDoesNotThrow(() -> processor.process(fields[0].getAnnotatedType().getDeclaredAnnotations()[0], b));
        assertThrows(IllegalArgumentException.class,
                () -> processor.process(fields[1].getAnnotatedType().getDeclaredAnnotations()[0], lon));

        try {
            processor.process(fields[1].getAnnotatedType().getDeclaredAnnotations()[0], lon);
        } catch (IllegalArgumentException ex) {
            assertEquals("Value must be from 2 to 5", ex.getMessage());
        }

        assertThrows(IllegalArgumentException.class,
                () -> processor.process(fields[2].getAnnotatedType().getDeclaredAnnotations()[0], str));

        try {
            processor.process(fields[2].getAnnotatedType().getDeclaredAnnotations()[0], str);
        } catch (IllegalArgumentException ex) {
            assertEquals("Object of this type can't have an InRange Annotation", ex.getMessage());
        }

        assertDoesNotThrow(() -> processor.process(fields[3].getAnnotatedType().getDeclaredAnnotations()[0], integer));

        assertDoesNotThrow(() -> processor.process(fields[4].getAnnotatedType().getDeclaredAnnotations()[0], sh));
        assertDoesNotThrow(() -> processor.process(fields[5].getAnnotatedType().getDeclaredAnnotations()[0], num));
    }
}