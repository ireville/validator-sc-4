/*
 * @author <a href="mailto:ivzyl@edu.hse.ru"> Iryna Zyl </a>
 */

package annotations.processors;

import annotations.Negative;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;

import static org.junit.jupiter.api.Assertions.*;

class NegativeProcessorTest {
    @Negative
    static byte b = -5;

    @Negative
    static Long lon = 172L;

    @Negative
    static String str = "1768";

    @Negative
    static int integer = 300;

    @Negative
    static short sh = -19;

    @Negative
    static String empty;

    @Test
    void process() {
        NegativeProcessor processor = new NegativeProcessor();
        Field[] fields = this.getClass().getDeclaredFields();

        assertDoesNotThrow(() -> processor.process(fields[0].getAnnotatedType().getDeclaredAnnotations()[0], b));
        assertThrows(IllegalArgumentException.class,
                () -> processor.process(fields[1].getAnnotatedType().getDeclaredAnnotations()[0], lon));

        try {
            processor.process(fields[1].getAnnotatedType().getDeclaredAnnotations()[0], lon);
        } catch (IllegalArgumentException ex) {
            assertEquals("Must be negative", ex.getMessage());
        }

        assertThrows(IllegalArgumentException.class,
                () -> processor.process(fields[2].getAnnotatedType().getDeclaredAnnotations()[0], str));

        try {
            processor.process(fields[2].getAnnotatedType().getDeclaredAnnotations()[0], str);
        } catch (IllegalArgumentException ex) {
            assertEquals("Object of this type can't have a Negative Annotation", ex.getMessage());
        }

        assertThrows(IllegalArgumentException.class,
                () -> processor.process(fields[3].getAnnotatedType().getDeclaredAnnotations()[0], integer));

        try {
            processor.process(fields[3].getAnnotatedType().getDeclaredAnnotations()[0], lon);
        } catch (IllegalArgumentException ex) {
            assertEquals("Must be negative", ex.getMessage());
        }

        assertDoesNotThrow(() -> processor.process(fields[4].getAnnotatedType().getDeclaredAnnotations()[0], sh));
        assertDoesNotThrow(() -> processor.process(fields[5].getAnnotatedType().getDeclaredAnnotations()[0], empty));
    }
}