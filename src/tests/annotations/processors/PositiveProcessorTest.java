/*
 * @author <a href="mailto:ivzyl@edu.hse.ru"> Iryna Zyl </a>
 */

package annotations.processors;

import annotations.Positive;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;

import static org.junit.jupiter.api.Assertions.*;

class PositiveProcessorTest {
    @Positive
    static byte b = 25;

    @Positive
    static Long lon = -62L;

    @Positive
    static String str = "1460";

    @Positive
    static int integer = -98;

    @Positive
    static short sh = 255;

    @Test
    void process() {
        PositiveProcessor processor = new PositiveProcessor();
        Field[] fields = this.getClass().getDeclaredFields();

        assertDoesNotThrow(() -> processor.process(fields[0].getAnnotatedType().getDeclaredAnnotations()[0], b));
        assertThrows(IllegalArgumentException.class,
                () -> processor.process(fields[1].getAnnotatedType().getDeclaredAnnotations()[0], lon));

        try {
            processor.process(fields[1].getAnnotatedType().getDeclaredAnnotations()[0], lon);
        } catch (IllegalArgumentException ex) {
            assertEquals("Must be positive", ex.getMessage());
        }

        assertThrows(IllegalArgumentException.class,
                () -> processor.process(fields[2].getAnnotatedType().getDeclaredAnnotations()[0], str));

        try {
            processor.process(fields[2].getAnnotatedType().getDeclaredAnnotations()[0], str);
        } catch (IllegalArgumentException ex) {
            assertEquals("Object of this type can't have a Positive Annotation", ex.getMessage());
        }

        assertThrows(IllegalArgumentException.class,
                () -> processor.process(fields[3].getAnnotatedType().getDeclaredAnnotations()[0], integer));

        try {
            processor.process(fields[3].getAnnotatedType().getDeclaredAnnotations()[0], lon);
        } catch (IllegalArgumentException ex) {
            assertEquals("Must be positive", ex.getMessage());
        }

        assertDoesNotThrow(() -> processor.process(fields[4].getAnnotatedType().getDeclaredAnnotations()[0], sh));
    }
}