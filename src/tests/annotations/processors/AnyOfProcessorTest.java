/*
 * @author <a href="mailto:ivzyl@edu.hse.ru"> Iryna Zyl </a>
 */

package annotations.processors;

import annotations.AnyOf;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;

import static org.junit.jupiter.api.Assertions.*;

class AnyOfProcessorTest {
    @AnyOf({"Hello", "Goodbye"})
    static String str = "Hello";

    @AnyOf({"Hello", "Goodbye"})
    static String str2 = "Afternoon";

    @AnyOf({"Hello", "Goodbye"})
    static Integer num = 5;

    @AnyOf({"Hello", "Goodbye"})
    static String empty;

    @Test
    void process() {
        AnyOfProcessor processor = new AnyOfProcessor();
        Field[] fields = this.getClass().getDeclaredFields();

        assertDoesNotThrow(() -> processor.process(fields[0].getAnnotatedType().getDeclaredAnnotations()[0], str));
        assertThrows(IllegalArgumentException.class,
                () -> processor.process(fields[1].getAnnotatedType().getDeclaredAnnotations()[0], str2));

        try {
            processor.process(fields[1].getAnnotatedType().getDeclaredAnnotations()[0], str2);
        } catch (IllegalArgumentException ex) {
            assertEquals("Must be one of the: 'Hello', 'Goodbye'", ex.getMessage());
        }

        assertThrows(IllegalArgumentException.class,
                () -> processor.process(fields[2].getAnnotatedType().getDeclaredAnnotations()[0], num));

        try {
            processor.process(fields[2].getAnnotatedType().getDeclaredAnnotations()[0], num);
        } catch (IllegalArgumentException ex) {
            assertEquals("Object of this type can't have an AnyOf Annotation", ex.getMessage());
        }

        assertDoesNotThrow(() -> processor.process(fields[3].getAnnotatedType().getDeclaredAnnotations()[0], empty));
    }
}