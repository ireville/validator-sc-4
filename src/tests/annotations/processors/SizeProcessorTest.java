/*
 * @author <a href="mailto:ivzyl@edu.hse.ru"> Iryna Zyl </a>
 */

package annotations.processors;

import annotations.Size;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

class SizeProcessorTest {
    @Size(min = 0, max = 7)
    static String str = "Hi";

    @Size(min = 2, max = 5)
    static String str2 = "Goodbye";

    @Size(min = 0, max = 100)
    static int integer = 3;

    @Size(min = 0, max = 3)
    static List<Short> list = new ArrayList<>();

    @Size(min = 1, max = 9)
    static Map<Long, Long> map = new HashMap<>();

    @Size(min = 2, max = 2)
    static Set<Integer> set = new LinkedHashSet<>();

    @Test
    void process() {
        SizeProcessor processor = new SizeProcessor();
        Field[] fields = this.getClass().getDeclaredFields();

        assertDoesNotThrow(() -> processor.process(fields[0].getAnnotatedType().getDeclaredAnnotations()[0], str));
        assertThrows(IllegalArgumentException.class,
                () -> processor.process(fields[1].getAnnotatedType().getDeclaredAnnotations()[0], str2));

        try {
            processor.process(fields[1].getAnnotatedType().getDeclaredAnnotations()[0], str2);
        } catch (IllegalArgumentException ex) {
            assertEquals("Object size must be from 2 to 5", ex.getMessage());
        }

        assertThrows(IllegalArgumentException.class,
                () -> processor.process(fields[2].getAnnotatedType().getDeclaredAnnotations()[0], integer));

        try {
            processor.process(fields[2].getAnnotatedType().getDeclaredAnnotations()[0], integer);
        } catch (IllegalArgumentException ex) {
            assertEquals("Object of this type can't have a Size Annotation", ex.getMessage());
        }

        assertDoesNotThrow(() -> processor.process(fields[3].getAnnotatedType().getDeclaredAnnotations()[0], list));

        assertThrows(IllegalArgumentException.class,
                () -> processor.process(fields[4].getAnnotatedType().getDeclaredAnnotations()[0], map));

        try {
            processor.process(fields[4].getAnnotatedType().getDeclaredAnnotations()[0], map);
        } catch (IllegalArgumentException ex) {
            assertEquals("Object size must be from 1 to 9", ex.getMessage());
        }

        set.add(3);
        set.add(90);
        assertDoesNotThrow(() -> processor.process(fields[5].getAnnotatedType().getDeclaredAnnotations()[0], set));
    }
}