package annotations.processors;

import annotations.NotNull;
import org.junit.jupiter.api.Test;

/*
 * @author <a href="mailto:ivzyl@edu.hse.ru"> Iryna Zyl </a>
 */

import java.lang.reflect.Field;

import static org.junit.jupiter.api.Assertions.*;

class NotNullProcessorTest {
    @NotNull
    static String str = "Hello";

    @NotNull
    static String str2 = "";

    @NotNull
    static Integer num = 5;

    @NotNull
    static String str3 = "        ";

    @NotNull
    static String str4 = null;

    @Test
    void process() {
        NotNullProcessor processor = new NotNullProcessor();
        Field[] fields = this.getClass().getDeclaredFields();

        assertDoesNotThrow(() -> processor.process(fields[0].getAnnotatedType().getDeclaredAnnotations()[0], str));
        assertThrows(IllegalArgumentException.class,
                () -> processor.process(fields[4].getAnnotatedType().getDeclaredAnnotations()[0], str4));

        try {
            processor.process(fields[4].getAnnotatedType().getDeclaredAnnotations()[0], str2);
        } catch (IllegalArgumentException ex) {
            assertEquals("Can't be null", ex.getMessage());
        }

        assertDoesNotThrow(() -> processor.process(fields[2].getAnnotatedType().getDeclaredAnnotations()[0], num));

        assertDoesNotThrow(() -> processor.process(fields[3].getAnnotatedType().getDeclaredAnnotations()[0], str3));
        assertDoesNotThrow(() -> processor.process(fields[1].getAnnotatedType().getDeclaredAnnotations()[0], str2));
    }
}