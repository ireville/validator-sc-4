/*
 * @author <a href="mailto:ivzyl@edu.hse.ru"> Iryna Zyl </a>
 */

package entities;

import annotations.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Constrained
public class MyForm {
    @NotBlank
    String str = "";

    @InRange(min = 5, max = 19)
    Integer integer = 20;

    @Positive
    Long lon = -92L;

    @Negative
    Short shor = 1;

    @Size(min = 10, max = 11)
    String str2 = "hello";

    @NotEmpty
    Map<Integer, String> map = new HashMap<>();

    @NotNull
    @NotEmpty
    List<@NotNull @NotEmpty List<@NotBlank String>> list = List.of(List.of(""));
}
