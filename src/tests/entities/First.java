/*
 * @author <a href="mailto:ivzyl@edu.hse.ru"> Iryna Zyl </a>
 */

package entities;

import annotations.Constrained;
import annotations.Negative;
import annotations.Positive;
import annotations.Size;

import java.util.List;

@Constrained
public class First {
    @Positive
    public int num = -1000000;

    public List<List<@Negative Integer>> list = List.of(List.of(5, -10, 20, -90));

    @Size(min = 0, max = 2)
    public List<Second> second = List.of(new Second());
}
