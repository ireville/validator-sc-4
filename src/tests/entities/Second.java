/*
 * @author <a href="mailto:ivzyl@edu.hse.ru"> Iryna Zyl </a>
 */

package entities;

import annotations.Constrained;
import annotations.InRange;

@Constrained
public class Second {
    @InRange(min = 2000, max = 3000)
    public int num = 300;
}
